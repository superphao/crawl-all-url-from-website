import { Kafka } from 'kafkajs'

export default class KafkaEventConsumer {

    constructor(
        brokers = [],
        topics = [],
        group,

    ) {
        this.brokers = brokers;
        this.topics = topics;
        this.group = group;
        this.consumer = this.create_consumer();
        this.is_consuming = false;
        this.recived_messages = [];
    }

    create_consumer() {
        const kafka = new Kafka({
            clientId: 'client-id',
            brokers: this.brokers,
        })
        const consumer = kafka.consumer({
            groupId: this.group,
            allowAutoTopicCreation: false
        })

        return consumer
    }

    async connect() {
        const topic = {
            topics: this.topics,
            fromBeginning: true,
        }

        try {
            await this.consumer.connect()
            await this.consumer.subscribe(topic)
            console.log('Consumer connected!');
        } catch (error) {
            console.log('Error: ', error)
        }
    }

    async start_consuming() {

        let a

        try {
            await this.connect()
            this.is_consuming = true
            await this.consumer.run({
                autoCommit: true,
                eachMessage: async (messagePayload) => {
                    const { topic, partition, message } = messagePayload
                    const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`
                    console.log(`- ${prefix} ${message.key}#${message.value}`)

                    await this.handle(message)
                    // a.push(message)
                }
            }).then(console.log)
        } catch (error) {
            this.is_consuming = false
            console.log('Error: ', error)
        }
    }

    async start_batch_consuming() {

        try {
            await this.connect()

            await this.consumer.run({
                eachBatch: async (eachBatchPayload) => {
                    const { batch } = eachBatchPayload
                    for (const message of batch.messages) {
                        const prefix = `${batch.topic}[${batch.partition} | ${message.offset}] / ${message.timestamp}`
                        console.log(`- ${prefix} ${message.key}#${message.value}`)

                        await this.handle(message)
                    }
                }
            })
        } catch (error) {
            console.log('Error: ', error)
        }
    }

    async disconnect(timeout = 60) {

        let consumedTopicPartitions = {}
        this.consumer.on(this.consumer.events.GROUP_JOIN, async ({ payload }) => {
            const { memberAssignment } = payload
            consumedTopicPartitions = Object.entries(memberAssignment).reduce(
                (topics, [topic, partitions]) => {
                    for (const partition in partitions) {
                        topics[`${topic}-${partition}`] = false
                    }
                    return topics
                },
                {}
            )
        })

        let processedBatch = true;
        this.consumer.on(this.consumer.events.FETCH_START, async () => {
            await new Promise(r => setTimeout(r, 1));
            this.consumer.disconnect();
            // process.exit(0);

            processedBatch = false;
        });

        /*
        * Now whenever we have finished processing a batch, we'll update `consumedTopicPartitions`
        * and exit if all topic-partitions have been consumed,
        */
        this.consumer.on(this.consumer.events.END_BATCH_PROCESS, async ({ payload }) => {
            const { topic, partition, offsetLag } = payload
            consumedTopicPartitions[`${topic}-${partition}`] = offsetLag === '0'

            if (Object.values(consumedTopicPartitions).every(consumed => Boolean(consumed))) {
                await this.consumer.disconnect()
                // process.exit(0)
            }

            processedBatch = true
        })

        // let waited = 0
        // console.log("Waiting for disconnect!");
        // console.log(this.is_consuming)

        // while (this.is_consuming == false && waited < timeout) {
        //     try {
        //         await this.consumer.disconnect()
        //         console.log("Disconnected!");
        //         break
        //     } catch (error) {
        //         console.log('Error: ', error);
        //     }

        //     await new Promise((r) => setTimeout(r, 1000));
        //     waited += 1
        // }
    }

    async shut_down() {
        try {
            await this.consumer.disconnect()
            console.log("Disconnected!")
        } catch (error) {
            console.log('Error: ', error);
        }
    }

    async handle(message) {

        this.recived_messages.push(message)

        console.log("Handle message done!");

        return this.recived_messages
    }
}
