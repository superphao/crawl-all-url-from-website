import { Kafka, CompressionTypes } from 'kafkajs'

export default class KafkaEventConsumer {
    constructor(
        brokers = [],
        topic,

    ) {
        this.brokers = brokers;
        this.topic = topic;
        this.producer = this.create_producer();
    }

    create_producer() {
        const kafka = new Kafka({
            clientId: 'client-id',
            brokers: this.brokers,
        })
        const producer = kafka.producer({
            allowAutoTopicCreation: true,
            transactionTimeout: 30000
        })

        return producer
    }

    async connect() {

        try {
            await this.producer.connect()
            console.log('Producer connected');
        } catch (error) {
            console.log('Error: ', error)
        }
    }

    async send_message(messages, timeout = 60000) {
        const try_send_message = setInterval(async () => {
            await this.producer
                .send({
                    timeout: timeout,
                    topic: this.topic,
                    compression: CompressionTypes.GZIP,
                    messages: messages,
                })
                .then(console.log, clearInterval(try_send_message))
                .catch(e => console.error(`[example/producer] ${e.message}`, e))
        }, 3000)

        setTimeout(() => clearInterval(try_send_message), timeout)
    }

    async send_batch_message(messages = [], timeout = 60000) {
        const topicMessages = {
            topic: this.topic,
            messages: messages
        }

        const batch = {
            topicMessages: [topicMessages]
        }

        const try_send_message = setInterval(async () => {
            await this.producer.sendBatch(batch)
                .then(console.log)
                .catch(e => console.error(`[example/producer] ${e.message}`, e))
        }, 10000)

        setTimeout(() => clearInterval(try_send_message), timeout)
    }

    async disconnect() {
        try {
            await this.producer.disconnect()
        } catch (error) {
            console.log('Error: ', error)
        }
    }
}