import KafkaEventConsumer from "../kafka/consumer.js";
import KafkaEventProducer from "../kafka/producer.js";
import fs from "fs"

const data_filename = process.env.DATA_FILENAME
const brokers = JSON.parse(process.env.BROKERS)
const producer_topic = process.env.PRODUCER_TOPIC
const producer = new KafkaEventProducer(brokers, producer_topic)

let messages = []

async function onFileContent(message, totalLink) {
    messages.push(message)

    if (messages.length == 500) {
        await producer.connect()

        await producer.send_message(messages.slice(500, 1000))
    }
}

function readFiles(onFileContent) {
    fs.readFile(`data/${data_filename}.json`, async function (err, data) {
        if (err) {
            return;
        }
        let totalLink = data.length
        data = JSON.parse(data)
        data.map(async function (content) {
            let newMessage = {
                key: 'new_post',
                value: JSON.stringify({
                    url: content,
                    domain: process.env.PAGE_DOMAIN
                })
            }
            await onFileContent(newMessage, totalLink)
        })
    });
}

await readFiles(onFileContent)