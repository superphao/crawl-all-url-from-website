import fs from "fs"

const data_filename = process.env.DATA_FILENAME
let data = []

function onFileContent(content, readedFileCount, totalFile) {
    // Filter link
    const regex = /\//g
    const count = (content.match(regex) || []).length
    if (count <= 3 && content.length > 50) {
        data.push(content)
    }

    if (readedFileCount == totalFile) {
        var uSet = new Set(data);
        data = [...uSet]

        fs.writeFile(`data/${data_filename}.json`, JSON.stringify(data), err => {
            console.log(err);
            console.log(data);
        })
    }
}

function readFiles(onFileContent) {
    fs.readdir("./storage/datasets/default/", async function (err, filenames) {
        if (err) {
            return;
        }
        let readedFileCount = 0
        let totalFile = filenames.length
        filenames.map(function (filename) {
            fs.readFile("./storage/datasets/default/" + filename, 'utf-8', function (err, content) {
                content = JSON.parse(content)
                readedFileCount++
                onFileContent(content.url, readedFileCount, totalFile)
                return content.url
            });
        })
    });
}

readFiles(onFileContent)