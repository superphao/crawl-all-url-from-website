import { CheerioCrawler, Dataset } from 'crawlee';

const page_url = process.env.PAGE_URL

async function main() {
    const crawler = new CheerioCrawler({
        // Function called for each URL
        async requestHandler({ request, $, enqueueLinks, log }) {

            // Save results as JSON to ./storage/datasets/default
            await Dataset.pushData({ url: request.loadedUrl });

            await enqueueLinks();
        },
        // maxRequestsPerCrawl: 10, // Limitation for only 10 requests (do not use if you want to crawl a sitemap)
    });

    await crawler.run([page_url]);


    // const queue = await RequestQueue.open();
    // console.log(queue);
}

main()